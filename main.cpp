//DES
//PATRYCJA LEWANDOWSKA s176702

#include<iostream>
#include<fstream>
#include "std_lib_facilities.hpp"
#include <bitset>
#include <cstdlib>
#include <string>



using namespace std;

//GENEROWANIE KLUCZA
string generacja(int n){
        string klucz = "";

    for(int i = 0 ; i<n; i++)
    {
        int x = rand()%2;
       string str =to_string(x);
       klucz+=str;}

    return klucz;
}
//ZAMIANA BINARNYCH NA DECIMAL
int bin2dec (string binarna)
{
    int dziesietna = strtol(binarna.c_str(), NULL, 2);

    return dziesietna;
}
//ZAMIANA TESKTU NA BINARNY STRING
string TextToBinaryString(string words) {
    string binaryString = "";
    for (char& _char : words) {
        binaryString +=bitset<8>(_char).to_string();
    }

    return binaryString;
}

void addZeros(string& str, int n)
{
    for (int i = 0; i < n; i++) {
        str = "0" + str;
    }
}

string getXOR(string a, string b)
{


    int aLen = a.length();
    int bLen = b.length();

    if (aLen > bLen) {
        addZeros(b, aLen - bLen);
    }
    else if (bLen > aLen) {
        addZeros(a, bLen - aLen);
    }

    int len = max(aLen, bLen);

    string res = "";
    for (int i = 0; i < len; i++) {
        if (a[i] == b[i])
            res += "0";
        else
            res += "1";
    }

    return res;
}

bool sprawdzanie_powtorzen(int wylosowana_liczba , int tab[]  , int ile )
{if( ile <= 0 )
         return false;

    int i = 0;
    do
    {
        if( tab[ i ] == wylosowana_liczba )
             return true;

        i++;
    } while( i < ile );

    return false;
}

int losowanie_liczby(int n){

return n = rand()% n;

}

vector<int> permutacja( int n){ // PERMUTACJA TYLKO WTEDY KIEDY CHCEMY ZMNIJESZYC CIAG
     int zakres = (n-8);
    int tab_element[zakres];
    vector<int> liczby_permutacji;

    int wylosowanych =0;
    int liczba;

do{
      liczba = losowanie_liczby(n);

        if(sprawdzanie_powtorzen( liczba , tab_element , wylosowanych)== false){
            tab_element[wylosowanych] = liczba;
            liczby_permutacji.push_back( tab_element[wylosowanych]);
            wylosowanych++;
        }


         }while(wylosowanych < zakres);

            return liczby_permutacji;


}

vector<int> permutacja_bez_zmiany(int n){
int tekst[n];
    int poczatek =0;
    int number;
    vector<int> permutacja;


   do{
      number = losowanie_liczby(n);

        if(sprawdzanie_powtorzen( number , tekst , poczatek)== false){
            tekst[poczatek] = number;
            permutacja.push_back( tekst[poczatek]);

            poczatek++;
        }}while(poczatek < n);

         return permutacja;
}
 string S_BOX( string prawa_strona, string key_48 , string lewa_strona  ){

vector<string> blok_po_s_box;
string ciag_32;
vector<string> S_box;

//PERMUTACJA ROZSZERZAJACA PRAWA STRONE
vector<int> liczby_permutacji;
liczby_permutacji = permutacja(32);


string pierwsza_prawa = prawa_strona;

        for(int i =0 ; i <16 ; i++)
        {

            pierwsza_prawa += pierwsza_prawa[liczby_permutacji[i]];
        }


//cout<<endl<<"prawa strona tekstu po rozszerzeniu do 48"<<endl<<pierwsza_prawa<<endl;


string prawa_i_klucz =getXOR(pierwsza_prawa , key_48);


//cout<< prawa_i_klucz;

string jeden , dwa , trzy , cztery , piec , szesc , siedem, osiem ;



for (int i =0 ; i<6; i++){


    jeden+=prawa_i_klucz[i];
    dwa += prawa_i_klucz[i+6];
    trzy += prawa_i_klucz[i+12];
   cztery += prawa_i_klucz[i+18];
   piec += prawa_i_klucz[i+24];
   szesc += prawa_i_klucz[i+30];
    siedem += prawa_i_klucz[i+36];
    osiem+= prawa_i_klucz[i+40];

}
S_box.push_back(jeden);
S_box.push_back(dwa);
S_box.push_back(trzy);
S_box.push_back(cztery);
S_box.push_back(piec);
S_box.push_back(szesc);
S_box.push_back(siedem);
S_box.push_back(osiem);


int box[8][4][16]= {{{14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},{0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},{4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},{15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}},
                    {{15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},{3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},{0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},{13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}},
                    {{10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},{13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},{13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},{1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}},
                    {{7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},{13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},{10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},{3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}},
                    {{2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},{14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},{4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},{11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}},
                    {{12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},{10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},{9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},{4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}},
                    {{4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},{13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},{1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},{6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}},
                    {{13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},{1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},{7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},{2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}}};


string dwubitowy_ciag;
string czterobitowy_ciag;
string Sbox;

for(int g=0 ; g<S_box.size(); g++){

    Sbox = S_box[g];
    dwubitowy_ciag="";
    czterobitowy_ciag ="";

        for(int i =1 ; i< 5 ;i++){

            czterobitowy_ciag+=Sbox[i];}

dwubitowy_ciag += Sbox[0];
dwubitowy_ciag +=Sbox[5];

//cout<<g<<" ciag dwubitowy "<<dwubitowy_ciag<<"\t ciag czterobitowy "<<czterobitowy_ciag<<endl<<endl;

int dwu_liczba = bin2dec(dwubitowy_ciag);

//cout<<"dwubitowa na dziesitna "<<dwu_liczba<<endl<<endl;

int cztero_liczba = bin2dec(czterobitowy_ciag);

//cout<<"czterobitowa na dziesitna "<<cztero_liczba<<endl<<endl;
int liczba_z_boxu = box[g][dwu_liczba][cztero_liczba];

//cout<<"element w tablicy S_box "<<liczba_z_boxu<<endl<<endl;


string decimal_na_binary = bitset<4>(liczba_z_boxu).to_string();
//cout<<"liczba z boxu na binarna "<< decimal_na_binary;


    ciag_32 +=decimal_na_binary;


}

 //cout<<endl<<"ciag 32 "<<ciag_32<<endl;

 //PERMUTACJA CIAGU 32
 vector<int> pertumacje_ciagu_32;
 pertumacje_ciagu_32 = permutacja_bez_zmiany(32);

string ciag_32_po_permutacji;

        for(int i =0 ; i <32 ; i++)
        {

            ciag_32_po_permutacji +=ciag_32[pertumacje_ciagu_32[i]];
        }
      //  cout<<"ciag po permutacji "<<ciag_32_po_permutacji<<endl;

string Xor_lewej_strony_ciagu_32 = getXOR(lewa_strona, ciag_32_po_permutacji);
//cout<<"Xor lewej strony ciagu 32 : "<<Xor_lewej_strony_ciagu_32<<endl;

//DODAWANIE STRON

string nowa_prawa = Xor_lewej_strony_ciagu_32;

string dodane_strony ="";
dodane_strony += prawa_strona;
dodane_strony +=nowa_prawa;

//cout<<"dodane strony : "<<dodane_strony;

liczby_permutacji.clear();
liczby_permutacji = permutacja_bez_zmiany(64);

string tekst_64_po_permutacji_koncowej;

 for(int i =0 ; i <64 ; i++)
        {

            tekst_64_po_permutacji_koncowej +=dodane_strony[liczby_permutacji[i]];
        }

        return tekst_64_po_permutacji_koncowej;


 }

int main()
{
    srand(time(NULL));
	ifstream odczyt;
    fstream zaszyfrowany_tekst;
    odczyt.open("plik.txt", ios::in);

    vector<char> input;
    vector<string> input_porcje;
    vector<string> tekst_zaszyfrowany;
    vector<string> tekst;
    string linia;


//GENEROWANIE KLUCZA
    string key ;
    key = generacja(64);

    cout<<"KLUCZ:"<<key<<endl;

    vector<int> liczby_permutacji = permutacja(64);
//KLUCZ PO PERMUTACJI 56
        cout<<endl<<"klucz po permutacji 56";
        string key_56;

        for(int i =0 ; i <56 ; i++)
        {

            key_56 +=key[liczby_permutacji[i]];
        }
        cout<<key_56<<endl;

//KLUCZ PRZESUNIETY W LEWO O JEDEN
cout<<endl;

        string przesunietyKlucz = key_56;
        przesunietyKlucz.erase(0,1); // WYCINA ELEMENT OD ZERA DO JEDEN
        przesunietyKlucz += key_56.at(0); //DODAJE ELEMENT

        cout<<endl<<"klucz po przesunieciu:" << przesunietyKlucz;

//48 bit KLUCZA
    liczby_permutacji.clear();
    liczby_permutacji = permutacja(56);


string key_48 ="";
cout<<endl<<"klucz 48 bit";

        for(int i =0 ; i <48 ; i++)
        {
            key_48 +=przesunietyKlucz[liczby_permutacji[i]];
        }
        cout<<key_48;


cout<<endl;

//TEKST
	if(!odczyt.is_open()) cout<<("Nie udalo sie otworzyc  pliku");

        while(getline(odczyt, linia)){
            for(int i = 0; i < linia.size(); i++){
                input.push_back(linia[i]);

                }}
		odczyt.close();
        string porcja;
//DZIELENIE ZNAKOW PO 8 I DOPELNIANIE ZERAMI

        int textSize = input.size();
        int zerosCount = 8 - (textSize % 8);
        if(textSize%8 != 0){
        for(int i = 0; i < zerosCount; i++){
            input.push_back('0');
        }}

		for(int i =0 ; i<input.size(); i++)
		{
		    porcja += input[i];

		    if((i+1)%8 == 0 ){
            input_porcje.push_back(porcja);
            porcja = "";
            }
		}

		cout<<endl;

		for(int i = 0; i < input_porcje.size(); i++){
            cout << "Linia " << i << " " << input_porcje[i] << endl;
		}

//ZAMIANA NA STRING BINARY

        cout<<endl<<"wiadomosc binarnie"<<endl;

		for( int i = 0 ; i<input_porcje.size() ; i++){

            cout<<"linia "<<i<<" "<<(TextToBinaryString(input_porcje[i]))<<endl;
           tekst.push_back(TextToBinaryString(input_porcje[i]));}

 //PERMUTACJA TEKSTU
        vector<int> permutacja_dla_tekstu;

       string slowko ;
        string tekst_permutacji;
        vector<string> tekst_po_permutacji;

        for(int k =0 ; k<tekst.size(); k++){

            slowko = tekst[k];
            tekst_permutacji = "";

            permutacja_dla_tekstu.clear();
             permutacja_dla_tekstu = permutacja_bez_zmiany(64);

                for( int i=0 ; i<slowko.size() ; i++){
                    tekst_permutacji += slowko[permutacja_dla_tekstu[i]];

               }

               tekst_po_permutacji.push_back(tekst_permutacji);

               }

    cout<<"\n permutacja tekstu \n";
    for(int i =0 ; i<tekst_po_permutacji.size(); i++){


        cout<<i<<" "<<tekst_po_permutacji[i]<<endl;
    }



//DZIELENIE TEKSTU NA STRONE LEWA I PRAWA
vector<string> prawa_strona;
vector<string> lewa_strona;

//cout<<endl<<"dzielenie tekstu na strony"<<endl;

for(int p =0 ; p<tekst_po_permutacji.size(); p++){

string lewa_strona_tekstu = "";
string prawa_strona_tekstu = "";
    string slowo ="";

    slowo = tekst_po_permutacji[p];

 for(int k =0 ; k<32; k++){

            lewa_strona_tekstu += slowo[k];
            prawa_strona_tekstu += slowo[k+32];

                }

                prawa_strona.push_back( prawa_strona_tekstu);
                lewa_strona.push_back(lewa_strona_tekstu);

//cout<<" lewa strona tekstu: "<<lewa_strona_tekstu<<endl<<" prawa strona tekstu: "<<prawa_strona_tekstu;
}

 vector<string> kryptogram_binarny;
        for(int b =0 ; b<lewa_strona.size() ; b++){

            string szyfr_binarny = "";

            szyfr_binarny = S_BOX( prawa_strona[b], key_48 , lewa_strona[b]);

            kryptogram_binarny.push_back(szyfr_binarny); }
            cout<<endl;

            for(int w =0 ; w<kryptogram_binarny.size(); w++){


                cout<<"szyfr koncowy binarny : "<<w<< " "<<kryptogram_binarny[w]<<endl;
            }




zaszyfrowany_tekst.open("zaszyfrowany_tekst_DES.txt", ios::out);

    for(int i =0; i<kryptogram_binarny.size() ; i++){


 istringstream in(kryptogram_binarny[i]);
    bitset<8> bin_to_string;
    while(in >> bin_to_string )
       zaszyfrowany_tekst<< char(bin_to_string.to_ulong());}

    zaszyfrowany_tekst.close();

    cout<<endl<<"wiadomosc zaszyfrowana przekonwertowana na typ string zapisana w pliku zaszyfrowany_tekst_DES.txt";




     cin.get();




    return 0;
}
